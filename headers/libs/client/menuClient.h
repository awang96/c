//
// Created by COICHOT on 07/06/2019.
//

#ifndef PROJET_MENUCLIENT_H
#define PROJET_MENUCLIENT_H

#include "client.h"
#include "../../commons/csvHelper.h"

void menuClient(FILE*);
void addClientMenu(FILE*);
void editClientMenu(FILE*);
Client editChoiceMenu(Client);
void removeClientMenu(FILE*);

#endif //PROJET_MENUCLIENT_H
