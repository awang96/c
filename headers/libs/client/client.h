//
// Created by COICHOT on 29/05/2019.
//

#ifndef PROJET_CLIENT_H
#define PROJET_CLIENT_H

#include "../../commons/csvHelper.h"

typedef struct client {
 int id;
 char name[55];
 char firstname[55];
 char job[155];
 char phoneNumber[11];
} Client;

Client newClient(int, char*, char*, char*, char*);
void printClient(Client);
void addClient(FILE*, Client);
FILE* editClient(FILE*, Client);
FILE* removeClient(FILE*, int);
Client getClientById(FILE*, int);

#endif //PROJET_CLIENT_H
