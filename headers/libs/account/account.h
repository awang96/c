//
// Created by ROCCA on 09/06/2019.
//

#ifndef PROJET_ACCOUNT_H
#define PROJET_ACCOUNT_H

typedef struct account {
    int id;
    int clientId;
    float balance;
    int accountTypeId;
    char creationDate[11];
} Account;

Account newAccount(int, int, float, int);
void printAccount(Account);
void addAccount(FILE*, Account);

#endif //PROJET_ACCOUNT_H
