//
// Created by COICHOT on 29/05/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../headers/commons/csvHelper.h"

int getId(FILE* f) {
    if(!f)
        return 0;
    char line[500];
    while(fgets(line, 500, f));
    return getIdFromLine(line);
}

int getIdFromLine(char *line) {
    char id[10];
    int idCharLen = strlen(line) - strlen(strchr(line, ','));
    strncpy(id, line, idCharLen);
    id[idCharLen] = '\0';
    return atoi(id);
}
