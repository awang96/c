//
// Created by COICHOT on 07/06/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../../../headers/libs/client/menuClient.h"

void menuClient(FILE* csv) {
    int choice;
    printf("Welcome to the client menu\n");
    printf("What do you wish to do ?\n");
    printf("1 - add a client\n");
    printf("2 - edit an existing client\n");
    printf("3 - remove a client\n");
    printf("Your choice:");
    scanf(" %d", &choice);
    switch(choice) {
        default:
            printf("Invalid choice\n");
            menuClient(csv);
            break;
        case 0:
            break;
        case 1:
            addClientMenu(csv);
            break;
        case 2:
            editClientMenu(csv);
            break;
        case 3:
            removeClientMenu(csv);
            break;
    }
}

void addClientMenu(FILE* csv) {
    int id;
    char name[55];
    char firstname[55];
    char job[155];
    char phoneNumber[11];
    id = getId(csv) + 1;
    printf("name:");
    scanf(" %s", &name);
    printf("firtname:");
    scanf(" %s", &firstname);
    printf("job:");
    scanf(" %s", &job);
    printf("phone number:");
    scanf(" %s", &phoneNumber);
    Client client = newClient(id, name, firstname, job, phoneNumber);
    if(!client.id) {
        printf("Invalid client data\n");
        addClientMenu(csv);
    } else {
        addClient(csv, client);
        fclose(csv);
        csv = fopen("../resources/clients.csv", "r+");
        printf("Client successfully added\n");
    }
    menuClient(csv);
}

void editClientMenu(FILE* csv) {
    int id;
    Client client;
    do {
        printf("client id:");
        scanf(" %d", &id);
        client = getClientById(csv, id);
    }while(!client.id);
    printf("Client found\n");
    client = editChoiceMenu(client);
    editClient(csv, client);
    fclose(csv);
    csv = fopen("../resources/clients.csv", "r+");
    menuClient(csv);
}

Client editChoiceMenu(Client client) {
    int choice;
    char name[55];
    char firstname[55];
    char job[155];
    char phoneNumber[11];
    printf("What do you wish to edit ?\n");
    printf("1 - name\n");
    printf("2 - firstname\n");
    printf("3 - job\n");
    printf("4 - phone number\n");
    printf("Your choice:");
    scanf(" %d", &choice);
    switch(choice) {
        default:
            printf("Invalid choice");
            editChoiceMenu(client);
            break;
        case 1:
            printf("name:");
            scanf(" %s", &name);
            strcpy(client.name, name);
            break;
        case 2:
            printf("firstname;");
            scanf(" %s", &firstname);
            strcpy(client.firstname, firstname);
            break;
        case 3:
            printf("job:");
            scanf(" %s", &job);
            strcpy(client.job, job);
            break;
        case 4:
            printf("phone number:");
            scanf(" %s", &phoneNumber);
            strcpy(client.phoneNumber, phoneNumber);
            break;
    }
    return client;
}

void removeClientMenu(FILE* csv) {
    int id;
    Client client;
    do {
        printf("client id:");
        scanf(" %d", &id);
        client = getClientById(csv, id);
    }while(!client.id);
    printf("Client found\n");
    removeClient(csv, client.id);
    fclose(csv);
    csv = fopen("../resources/clients.csv", "r+");
    printf("Client removed\n");
    menuClient(csv);
}