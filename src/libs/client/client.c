//
// Created by COICHOT on 29/05/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windef.h>
#include <stdint.h>
#include "../../../headers/libs/client/client.h"

Client newClient(int id, char* name, char* firstname, char* job,char* phoneNumber ) {
    Client client;
    int i;
    client.id = 0;
    if(id <= 0 || !name || !firstname || !job || !phoneNumber) {
        return client;
    }
    client.id = id;
    strcpy(client.name, name);
    strcpy(client.firstname, firstname);
    strcpy(client.job, job);
    for(i = 0; phoneNumber[i] != '\0'; i++) {
        if(phoneNumber[i] > '9' || phoneNumber[i] < '0') {
            client.id = 0;
            return client;
        }
    }
    if(i < 10) {
        client.id = 0;
        return client;
    }
    strcpy(client.phoneNumber, phoneNumber);
    return client;
}

void printClient(Client client) {
    printf("id: %d | name: %s | firstname: %s | job: %s | phoneNumber: %s\n",
            client.id,
            client.name,
            client.firstname,
            client.job,
            client.phoneNumber);
}

void addClient(FILE* f, Client client) {
    fseek(f, 0, SEEK_END);
    fprintf(f, "%d,%s,%s,%s,%s\n",
            client.id,
            client.name,
            client.firstname,
            client.job,
            client.phoneNumber);
}

FILE* editClient(FILE* f, Client editedClient) {
    fseek(f, 0, SEEK_SET);
    char line[500];
    FILE *editedFile = fopen("../resources/tmp.csv", "w");
    fgets(line, 500, f);
    fprintf(editedFile, "%s", line);
    while(fgets(line, 500, f)) {
        if(getIdFromLine(line) == editedClient.id) {
            fprintf(editedFile, "%d,%s,%s,%s,%s\n",
                    editedClient.id,
                    editedClient.name,
                    editedClient.firstname,
                    editedClient.job,
                    editedClient.phoneNumber);
        }
        else {
            fprintf(editedFile, "%s", line);
        }
    }
    fclose(f);
    remove("../resources/clients.csv");
    fclose(editedFile);
    rename("../resources/tmp.csv", "../resources/clients.csv");
    return fopen("../resources/clients.csv", "r+");
}

FILE* removeClient(FILE *f, int id) {
    fseek(f, 0, SEEK_SET);
    char line[500];
    FILE* editedFile = fopen("../resources/tmp.csv", "w");
    fgets(line, 500, f);
    fprintf(editedFile, "%s", line);
    while(fgets(line, 500, f)) {
        if(getIdFromLine(line) != id) {
            fprintf(editedFile, "%s", line);
        }
    }
    fclose(f);
    remove("../resources/clients.csv");
    fclose(editedFile);
    rename("../resources/tmp.csv", "../resources/clients.csv");
    return fopen("../resources/clients.csv", "r+");
}

Client getClientById(FILE* f, int id) {
    fseek(f, 0, SEEK_SET);
    char line[500];
    char found = 0;
    char name[55];
    char firstname[55];
    char job[155];
    char phoneNumber[11];
    Client client;
    fgets(line, 500, f);
    while(fgets(line, 500, f)) {
        if(getIdFromLine(line) == id) {
            found = 1;
            break;
        }
    }
    if(!found) {
        client.id = 0;
        return client;
    }
    char *p;
    p = strchr(line, ',') + 1;
    int nameCharLen = strlen(p) - strlen(strchr(p, ','));
    strncpy(name, p, nameCharLen);
    name[nameCharLen] = '\0';
    p = strchr(p, ',') + 1;
    int firstnameCharLen = strlen(p) - strlen(strchr(p, ','));
    strncpy(firstname, p, firstnameCharLen);
    firstname[firstnameCharLen] = '\0';
    p = strchr(p, ',') + 1;
    int jobCharLen = strlen(p) - strlen(strchr(p, ','));
    strncpy(job, p, jobCharLen);
    job[jobCharLen] = '\0';
    p = strchr(p, ',') + 1;
    int phoneCharLen = strlen(p) - strlen(strchr(p, '\n'));
    strncpy(phoneNumber, p, phoneCharLen);
    phoneNumber[phoneCharLen] = '\0';
    client = newClient(id, name, firstname, job, phoneNumber);
    return client;
}