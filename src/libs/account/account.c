//
// Created by ROCCA on 09/06/2019.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "../../../headers/libs/account/account.h"

Account newAccount(int id, int clientId, float balance, int accountTypeId) {
    Account account;
    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    account.id = id;
    account.clientId = clientId;
    account.balance = balance;
    account.accountTypeId = accountTypeId;
    sprintf(account.creationDate, "%d-%d-%d", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
    return account;
}

void printAccount(Account account) {
    printf("id: %d | clientId: %d | balance: %.2f | accountTypeId: %d | creation: %s",
            account.id,
            account.clientId,
            account.balance,
            account.accountTypeId,
            account.creationDate);
}

void addAccount(FILE* csv, Account account) {
    fseek(csv, 0, SEEK_END);
    fprintf(csv, "%d,%d,%.2f,%d,%s\n",
            account.id,
            account.clientId,
            account.balance,
            account.accountTypeId,
            account.creationDate);
}